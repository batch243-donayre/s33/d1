
//[section] JavaScript Synchronous vs Asynchronous
/*JavaScript is by default synchronous - means that only one statement can be executed at a time.
-read line by line.
*/


console.log("Hello world");
//console.log("Hello Again");
console.log("Goodbye");


//Code blocking

//Asynchronous means that we can proceed to execute other statement, while time consuming codes are running in the background.

//The Fetch API allows you to asynchronouly request for resource (data)
//A "promise" is an object that represents the eventual completion (or failure) of an asynchtonous function and its resulting value
//Syntax
	//fetch('URL')
	//.then((response)=>{})

//[CHECK THE STATUS OF THE REQUEST]
	//By using the .then method we can now check for the status of the promise

	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response => console.log(response.status));
	// The 'fetch' method will return a 'promise' that esolves to a 'response' object

	//The .then method captures the 'Response' object and returns another 'promise' which will eventually be 'resolved' or 'rejected'

//[RETRIEVE CONTENTS/DATA FROM THE RESPONSE OBJECT]

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => response.json())
//Using multiple 'then' method creates a 'promise chain'
.then((json)=>console.log(json))


//[CREATE A FUNCTION THAT WILL DEMONSTRATE USING 'async' and 'await' keyword]
//The 'async' and 'await' keyword is another approach that can be usd to achieve an asynchromous code
//Used in functio to indicate which portions of code should be awauted for 
//Creates an synchronous function

async function fetchData(){

	//waits for the 'fetch' method to complete then stores the value in the result variable
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	//Result returned by fetch is a 'promise'
	console.log(result);

	//The retuned 'response' is an object
	console.log(typeof result);

	//We can not access the content of the 'response' directly by accessing itsbody property
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}

fetchData();


//[RETRIVE A SPECIFIC POST]

	fetch('https://jsonplaceholder.typicode.com/posts/1')
	.then((response) => response.json())
	.then((json) => console.log(json))


//[CREATE POST]
	//SYNTAX:
		//fetch('URL', options)
		//.then((response) => {})
		//.then((response) => {})

	fetch('https://jsonplaceholder.typicode.com/posts',{

		//sets the method of 'request' object to POST
		method : 'POST',

		// Specified that the content will be in a JSON structure
		headers :{
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
			title: 'New Post',
			body: 'Hello world',
			userId: 1
		}),

	})

	.then((response) => response.json())
	.then((json) => console.log(json));

//[UPDATE A POST USING PUT METHOD]

	fetch('https://jsonplaceholder.typicode.com/posts/1',{

		method: 'PUT'
		headers:{
			'Content-type' : 'application/json',
		},
		body : JSON.stringify({
			id: 1,
			title: 'Updated post',
			body: 'Hello again!',
			userId: 1
		})
	})

	.then((response) => response.json())
	.then((json) => console.log(json));

//[UPDATE A POST USING PATCH METHOD]

	//PATCH is used to update the whole object
	//PUT is used to update a single property

	fetch('https://jsonplaceholder.typicode.com/posts/1',{

		method: 'PATCH',
		headers:{
			'Content-type' : 'application/json',
		},
		body: JSON.stringify({
			title: 'Corrected post'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));


//[DELETE  POST]

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method : "DELETE"
	})